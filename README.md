# AWIT-SSH-Client

awit-ssh is a perl script that automates connecting to a server via ssh by looking up the user and port information from a LDAP database.

## Setup

Just run awit-ssh and it will let you know if any of its dependencies are missing.

Install them and create a ~/.awit-ssh.conf configuration file containing your LDAP server details, e.g.:

```ini
[server]
uri=ldaps://server.example.com
base=dc=example,dc=com

[pkcs11]
# Smartcard provider
#provider=/usr/lib/x86_64-linux-gnu/pkcs11/libXXXXpkcs11.so
```

## Usage

General usage is:

```
awit-ssh hostname
```
